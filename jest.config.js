module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  coverageDirectory: 'coverage',
  collectCoverageFrom: ['<rootDir>/src/**/*.{ts,js}'],
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '<rootDir>/contrib/',
    '<rootDir>/coverage/',
    '<rootDir>/lib/',
    '<rootDir>/tests/',
    '<rootDir>/src/types.ts',
  ],
};
