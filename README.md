# Universa
This is an official Node.js module from [Universa](https://universablockchain.com/) to facilitate access to the Java library using Universa's UMI protocol.

## Get started

### Prerequisites
* Node 8+
* JVM v1.8* must be installed to run the UMI tool. Java runtimes for different OSes could be downloaded from https://www.java.com/en/download/.

### Installing
This library is distributed on [npm](https://www.npmjs.com/). In order to add it as a dependency, run the following command:

```shell
$ npm i --save universa-umi
```

## Usage
```javascript
const { PrivateKey, Contract } = require('universa-umi');

async function main () {
  const privateKey = await PrivateKey.create(2048);
  const contract = await Contract.create(privateKey);
  await contract.seal();
  console.log(`Contract is ok: ${await contract.check()}`);
  console.log(`Contract id: ${await (await contract.getId()).toString()}`)
}

main();
```

## Running the tests
* Clone the repo
  ```shell
  $ git clone https://gitlab.com/universa/universa.git && cd universa
  ```
* Install the dependencies
  ```shell
  $ npm install
  ```
* Run the tests:
  ```shell
  $ npm test
  ```

## Acknowledgments
* Universa Java API: https://kb.universablockchain.com/general_java_api/5
* Universa UMI server: https://kb.universablockchain.com/umi_protocol/98
* Universa Ruby gem: https://github.com/sergeych/universa
* Universa Python package: https://github.com/vkovrigin/universa

## Versioning
We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/universa/universa/tags).

## Built with
* [TypeScript](https://www.typescriptlang.org/)

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
