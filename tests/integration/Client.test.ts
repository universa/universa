import createDebug from 'debug';
import { Client } from '../../src/Client';
import { Connection } from '../../src/Connection';
import { Contract } from '../../src/Contract';
import { ContractState } from '../../src/ContractState';
import { PrivateKey } from '../../src/PrivateKey';
import { UMI } from '../../src/UMI';
import * as os from 'os';
import * as fs from 'fs';
import * as path from 'path';
import { promisify } from 'util';

const debug = createDebug('universa:integration');

describe('Client', () => {
  let tmpCacheDir: string;
  let privateKey: PrivateKey;
  let client: Client;

  beforeAll(async () => {
    tmpCacheDir = await promisify(fs.mkdtemp)(path.join(os.tmpdir(), 'universa-'));
    debug('Created temporary directory for topology cache: %s', tmpCacheDir);

    privateKey = await PrivateKey.fromPacked(Buffer.from(process.env.UNIVERSA_PRIVATE_KEY as string, 'base64'));
    client = await Client.create({
      topologyFile: process.env.UNIVERSA_TOPOLOGY_FILE as string,
      privateKey,
      cacheDir: tmpCacheDir,
    });
  });

  afterAll(async () => {
    const umi = (await (UMI as any)._getInstance()) as UMI;
    await umi.close();
  });

  it('should scan the network', async () => {
    expect(client).toBeInstanceOf(Client);
    expect(client.size).toBeGreaterThanOrEqual(10);

    const randomConnection = await client.getRandomConnection();
    expect(randomConnection).toBeInstanceOf(Connection);
    expect(await randomConnection.ping()).toBeTruthy();
    expect(await randomConnection.getUrl()).toMatch(/^http/);
    expect(randomConnection.pingNode(31)).rejects.toThrow();
  });

  it('should ping nodes', async () => {
    const [connection1, connection2] = await client.getRandomConnections(2);
    const res = await connection1.pingNode(await connection2.getNodeNumber());
    expect(await res.umi.invoke(res, 'get', 'TCP')).toBeGreaterThanOrEqual(0);
    expect(await res.umi.invoke(res, 'get', 'UDP')).toBeGreaterThanOrEqual(0);
  });

  it('should check state of unregistered contract', async () => {
    const contract = await Contract.create(privateKey);
    await contract.setExpiresAt(new Date(Date.now() + 9e5));
    const definition = await contract.getDefinition();
    await definition.set('name', 'just a test');
    await contract.seal();

    const connection = await client.getRandomConnection();
    const state = await connection.getState(contract);
    expect(state.state).toBe('UNDEFINED');
    expect(state.isPending).toBe(false);
    expect(state.isApproved).toBe(false);
  });

  it('should check consensus state of unregistered contract', async () => {
    const contract = await Contract.create(privateKey);
    await contract.setExpiresAt(new Date(Date.now() + 9e5));
    const definition = await contract.getDefinition();
    await definition.set('name', 'just a test');
    await contract.seal();

    const state = await client.getState(contract, 0.2);
    expect(state.state).toBe('UNDEFINED');
    expect(state.isPending).toBe(false);
    expect(state.isApproved).toBe(false);
  });

  // XXX: Skip for now, as revoke process is not clear
  it.skip('should register a new contract and revoke it', async () => {
    const key = await PrivateKey.create(2048);
    const contract = await Contract.create(key);
    await contract.setExpiresAt(new Date(Date.now() + 9e5));
    const definition = await contract.getDefinition();
    await definition.set('name', 'just a test');
    await contract.seal();

    const state = (await client.registerSingle(contract)) as ContractState;
    expect(state.isApproved).toBeTruthy();
    expect(state.errors).toHaveLength(0);
    expect(await client.isApproved(contract)).toBeTruthy();
    const hashId = await contract.getHashId();
    expect(await client.isApproved(hashId)).toBeTruthy();
    const hashIdString = await contract.getHashId();
    expect(await client.isApproved(hashIdString)).toBeTruthy();
    const bytes = await hashId.getBytes();
    expect(await client.isApproved(bytes)).toBeTruthy();

    const revocation = await contract.createRevocation(privateKey);
    const revocationState = (await client.registerSingle(revocation)) as ContractState;
    expect(revocationState.isApproved).toBeTruthy();

    const newState = await client.getState(contract, 0.2);
    expect(newState.state).toBe('REVOKED');
    expect(await client.isApproved(contract)).not.toBeTruthy();
  }, 10000);
});
