import * as assert from 'assert';
import { RemoteObjectRef } from '../../src/RemoteObjectRef';
import { UMI } from '../../src/UMI';
import { UmiRef } from '../../src/types';

describe('RemoteObjectRef', () => {
  it('should create an instance of RemoteObjectRef', async () => {
    const umi = {} as UMI;
    const ref = { id: 1, className: 'remote.class.name' } as UmiRef;
    const remoteObjectRef = new RemoteObjectRef({ umi, ref });
    assert(remoteObjectRef instanceof RemoteObjectRef);
    assert.strictEqual(remoteObjectRef.umi, umi);
    assert.strictEqual(remoteObjectRef.ref, ref);
    assert.strictEqual(remoteObjectRef.id, ref.id);
    assert.strictEqual(remoteObjectRef.remoteClassName, ref.className);
    assert.strictEqual(remoteObjectRef.getRef(umi), ref);
  });

  it(`should throw if '.getRef' called with wrong UMI instance`, async () => {
    const umi = {} as UMI;
    const ref = { id: 1, className: 'remote.class.name' } as UmiRef;
    const remoteObjectRef = new RemoteObjectRef({ umi, ref });

    assert.throws(() => {
      remoteObjectRef.getRef({} as UMI);
    });
  });
});
