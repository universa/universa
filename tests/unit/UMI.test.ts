/* eslint-disable @typescript-eslint/no-explicit-any */
import { UMI } from '../../src/UMI';
import { remoteCommandHandler } from './helpers';
import { RemoteObjectRef } from '../../src/RemoteObjectRef';
import { RemoteObject } from '../../src/RemoteObject';
import { UmiRef } from '../../src/types';

jest.mock('child_process');

describe('UMI', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.resetModules();
  });

  afterEach(async () => {
    jest.clearAllMocks();
  });

  it('should create an instance of UMI', async () => {
    const instance = await UMI.create();
    expect(instance).toBeInstanceOf(UMI);
  });

  it('should close the connection to UMI', async () => {
    const instance = await UMI.create();
    await instance.close();
    expect(instance.isClosed).toBe(true);
  });

  it('should return UMI version', async () => {
    const instance = await UMI.create();
    expect(instance.version).toStrictEqual('0.8.63');
  });

  it('should return Universa network library core version', async () => {
    const instance = await UMI.create();

    remoteCommandHandler.mockImplementationOnce(async (_, res: any) => {
      res.result = '3.0.0';
    });
    // As per the mock implementation, this will return the request itslef
    const coreVersion = await instance.getCoreVersion();

    expect(coreVersion).toStrictEqual('3.0.0');
  });

  it('should throw when the system is unsupported', async () => {
    await expect(UMI.create({ system: 'YUPPI' })).rejects.toThrowError(`Unsupported system: 'YUPPI'`);
  });

  it('should throw when the version is unsupported', async () => {
    await expect(UMI.create({ version: '1.0.0' })).rejects.toThrowError(`Unsupported version: '1.0.0'`);
  });

  it('should emit an error when subprocess is terminated from the outside', (done) => {
    UMI.create().then((instance) => {
      instance.on('error', (err) => {
        expect(err.message).toStrictEqual('UMI subprocess terminated from the outside.');
        done();
      });

      (instance as any)._subprocess.kill();
    });
  });

  it('should emit an error when subprocess is terminated abnormally', (done) => {
    UMI.create().then((instance) => {
      instance.on('error', (err) => {
        expect(err.message).toStrictEqual('UMI subprocess terminated abnormally. Code: 130. Signal: null.');
        done();
      });

      (instance as any)._subprocess.kill('SIGINT');
    });
  });

  it('should throw when making a call after UMI was closed', async () => {
    const instance = (await UMI.create()) as any;
    await instance.close();
    await expect(instance.call('version')).rejects.toThrowError('UMI interface is closed.');
  });

  it(`should call 'call' when executing 'instantiate' method`, async () => {
    const instance = (await UMI.create()) as any;
    const spy = jest.spyOn(instance as any, 'call') as jest.MockedFunction<any>;

    await instance.instantiate('RemoteClass', 'arg1', 'arg2');

    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith('instantiate', 'RemoteClass', 'arg1', 'arg2');
  });

  it(`should call the singleton instance's 'instantiate' method when static 'instantiate' method was called`, async () => {
    const singleton = await (UMI as any)._getInstance();
    const spy = jest.spyOn(singleton, 'instantiate');

    await UMI.instantiate('RemoteClass', 'arg1', 'arg2');

    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith('RemoteClass', 'arg1', 'arg2');
  });

  it(`should call the singleton instance's 'invoke' method when static 'invoke' method was called`, async () => {
    const singleton = await (UMI as any)._getInstance();
    const spy = jest.spyOn(singleton, 'invoke');
    const remoteObjectRef = {} as RemoteObjectRef;

    await UMI.invoke(remoteObjectRef, 'someMethod', 'arg1', 'arg2');

    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith(remoteObjectRef, 'someMethod', 'arg1', 'arg2');
  });

  it(`should call the singleton instance's 'invokeStatic' method when static 'invokeStatic' method was called`, async () => {
    const singleton = await (UMI as any)._getInstance();
    const spy = jest.spyOn(singleton, 'invokeStatic');

    await UMI.invokeStatic('RemoteClass', 'someMethod', 'arg1', 'arg2');

    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith('RemoteClass', 'someMethod', 'arg1', 'arg2');
  });

  it(`should call the singleton instance's 'getProperty' method when static 'getProperty' method was called`, async () => {
    const singleton = await (UMI as any)._getInstance();
    const spy = jest.spyOn(singleton, 'getProperty');
    const remoteObjectRef = { umi: singleton } as RemoteObjectRef;

    await UMI.getProperty(remoteObjectRef, 'someProperty');

    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith(remoteObjectRef, 'someProperty');
  });

  it(`should call the singleton instance's 'setProperty' method when static 'setProperty' method was called`, async () => {
    const singleton = await (UMI as any)._getInstance();
    const spy = jest.spyOn(singleton, 'setProperty');
    const remoteObjectRef = { umi: singleton } as RemoteObjectRef;

    await UMI.setProperty(remoteObjectRef, 'someProperty', 'someValue');

    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith(remoteObjectRef, 'someProperty', 'someValue');
  });

  it('should handle erroneous call result', async () => {
    const umi = (await UMI.create()) as any;

    remoteCommandHandler.mockImplementationOnce(async (_, res) => {
      delete res.result;
      res.error = { text: 'Erroneous request' };
    });

    await expect(umi.call('error')).rejects.toThrowError('Erroneous request');
  });

  it('should handle unknown call result', async () => {
    const umi = (await UMI.create()) as any;
    const spy = jest.spyOn(umi, '_debugCall');
    const response = { serial: 1, cmd: 'unknown', args: [] };
    const data = Buffer.from(JSON.stringify(response));

    umi.handleResponse(data);

    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith('<< UNEXPECTED %o', response);
  });

  describe('invoke() and invokeStatic()', () => {
    it(`should support 'invoke' for RemoteObjectRef`, async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const spy = jest.spyOn(umi, 'call');
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi, ref });

      await umi.invoke(remoteObjectRef, 'someMethod', 'arg1', 'arg2');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('invoke', ref.id, 'someMethod', 'arg1', 'arg2');
    });

    it(`should support 'invoke' for RemoteObject`, async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const spy = jest.spyOn(umi, 'call');
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi, ref });
      const remoteObject = await RemoteObject.create(remoteObjectRef);

      await umi.invoke(remoteObject, 'someMethod', 'arg1', 'arg2');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('invoke', ref.id, 'someMethod', 'arg1', 'arg2');
    });

    it(`should support 'invoke' for string argument (effectively 'invokeStatic')`, async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const spy = jest.spyOn(umi, 'call');

      await umi.invoke('RemoteClass', 'someStaticMethod', 'arg1', 'arg2');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('invoke', 'RemoteClass', 'someStaticMethod', 'arg1', 'arg2');
    });

    it('should throw when argument is an instance of RemoteObjectRef created by another UMI instance', async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi: {} as UMI, ref });

      await expect(umi.invoke(remoteObjectRef, 'someMethod', 'arg1', 'arg2')).rejects.toThrowError(
        "Objects can't be interchanged between different UMI interfaces",
      );
    });

    it('should throw when argument is an instance of RemoteObject created by another UMI instance', async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi: {} as UMI, ref });
      const remoteObject = await RemoteObject.create(remoteObjectRef);

      await expect(umi.invoke(remoteObject, 'someMethod', 'arg1', 'arg2')).rejects.toThrowError(
        "Objects can't be interchanged between different UMI interfaces",
      );
    });

    it(`should call 'invoke' when calling 'invokeStatic`, async () => {
      const umi = await UMI.create();
      const spy = jest.spyOn(umi, 'invoke');

      await umi.invokeStatic('RemoteClass', 'someStaticMethod', 'arg1', 'arg2');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('RemoteClass', 'someStaticMethod', 'arg1', 'arg2');
    });
  });

  describe('getProperty() and getField()', () => {
    it(`should support 'getProperty' for RemoteObjectRef`, async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const spy = jest.spyOn(umi, 'call');
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi, ref });

      await umi.getProperty(remoteObjectRef, 'someProperty');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('get_field', ref.id, 'someProperty');
    });

    it(`should support 'getProperty' for RemoteObject`, async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const spy = jest.spyOn(umi, 'call');
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi, ref });
      const remoteObject = await RemoteObject.create(remoteObjectRef);

      await umi.getProperty(remoteObject, 'someProperty');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('get_field', ref.id, 'someProperty');
    });

    it('should throw when argument is an instance of RemoteObjectRef created by another UMI instance', async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi: {} as UMI, ref });

      await expect(umi.getProperty(remoteObjectRef, 'someProperty')).rejects.toThrowError(
        "Objects can't be interchanged between different UMI interfaces",
      );
    });

    it('should throw when argument is an instance of RemoteObjectRef created by another UMI instance', async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi: {} as UMI, ref });
      const remoteObject = await RemoteObject.create(remoteObjectRef);

      await expect(umi.getProperty(remoteObject, 'someProperty')).rejects.toThrowError(
        "Objects can't be interchanged between different UMI interfaces",
      );
    });

    it(`should call 'getProperty' when calling 'getField'`, async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const spy = jest.spyOn(umi, 'getProperty');
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi, ref });

      await umi.getField(remoteObjectRef, 'someProperty');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(remoteObjectRef, 'someProperty');
    });
  });

  describe('setProperty() and setField()', () => {
    it(`should support 'getProperty' for RemoteObjectRef`, async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const spy = jest.spyOn(umi, 'call');
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi, ref });

      await umi.setProperty(remoteObjectRef, 'someProperty', 'someValue');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('set_field', ref.id, 'someProperty', 'someValue');
    });

    it(`should support 'getProperty' for RemoteObject`, async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const spy = jest.spyOn(umi, 'call');
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi, ref });
      const remoteObject = await RemoteObject.create(remoteObjectRef);

      await umi.setProperty(remoteObject, 'someProperty', 'someValue');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('set_field', ref.id, 'someProperty', 'someValue');
    });

    it('should throw when argument is an instance of RemoteObjectRef created by another UMI instance', async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi: {} as UMI, ref });

      await expect(umi.setProperty(remoteObjectRef, 'someProperty', 'someValue')).rejects.toThrowError(
        "Objects can't be interchanged between different UMI interfaces",
      );
    });

    it(`should call 'getProperty' when calling 'getField'`, async () => {
      const umi = (await UMI.create()) as any; // because 'call' is private
      const spy = jest.spyOn(umi, 'setProperty');
      const ref = { id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi, ref });

      await umi.setField(remoteObjectRef, 'someProperty', 'someValue');

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(remoteObjectRef, 'someProperty', 'someValue');
    });
  });

  describe('toUmiValue() and fromUmiValue()', () => {
    let umi: any;

    beforeAll(async () => {
      umi = await UMI.create();
    });

    it(`should transform values to UMI values`, async () => {
      expect(await umi.toUmiValue(null)).toEqual(null);
      expect(await umi.toUmiValue(undefined)).toEqual(null);
      expect(await umi.toUmiValue(true)).toEqual(true);
      expect(await umi.toUmiValue(false)).toEqual(false);
      expect(await umi.toUmiValue(42)).toEqual(42);
      expect(await umi.toUmiValue('string')).toEqual('string');

      const buf = Buffer.from('binary string');
      expect(await umi.toUmiValue(buf)).toStrictEqual({ __type: 'binary', base64: buf.toString('base64') });

      const date = new Date();
      expect(await umi.toUmiValue(date)).toStrictEqual({ __type: 'unixtime', seconds: (date.getTime() / 1000) | 0 });

      const ref = { __type: 'RemoteObject', id: 1 } as UmiRef;
      const remoteObjectRef = new RemoteObjectRef({ umi, ref });
      const remoteObject = await RemoteObject.create(remoteObjectRef);
      expect(await umi.toUmiValue(remoteObject)).toStrictEqual(ref);
      expect(await umi.toUmiValue(remoteObjectRef)).toStrictEqual(ref);

      const ref2 = { __type: 'RemoteObject', id: 2 } as UmiRef;
      const remoteObjectRef2 = new RemoteObjectRef({ umi, ref: ref2 });
      const set = new Set([remoteObject, remoteObjectRef2]);
      const spy = jest.spyOn(umi, 'call');
      await umi.toUmiValue(set);
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('instantiate', 'Set', [ref, ref2]);
      spy.mockRestore();

      const plainObject = {
        number: 42,
        string: 'string',
        boolean: false,
        null: null,
        undef: undefined,
        buffer: Buffer.from('binary string'),
        date: new Date(),
        array: [42, 'string', false, null, undefined, Buffer.from('binary string'), new Date()],
        object: { key: 42, value: 'string' },
      };
      expect(await umi.toUmiValue(plainObject)).toEqual({
        number: 42,
        string: 'string',
        boolean: false,
        null: null,
        undef: null,
        buffer: { __type: 'binary', base64: plainObject.buffer.toString('base64') },
        date: { __type: 'unixtime', seconds: (plainObject.date.getTime() / 1000) | 0 },
        array: [
          42,
          'string',
          false,
          null,
          null,
          { __type: 'binary', base64: plainObject.buffer.toString('base64') },
          { __type: 'unixtime', seconds: (plainObject.date.getTime() / 1000) | 0 },
        ],
        object: { key: 42, value: 'string' },
      });
    });

    it('should transform values from UMI values', async () => {
      expect(await umi.fromUmiValue(null)).toEqual(null);
      expect(await umi.fromUmiValue(true)).toEqual(true);
      expect(await umi.fromUmiValue(false)).toEqual(false);
      expect(await umi.fromUmiValue(42)).toEqual(42);
      expect(await umi.fromUmiValue('string')).toEqual('string');
      expect(await umi.fromUmiValue({ __type: 'binary', base64: 'YmluYXJ5IHN0cmluZw==' })).toEqual(
        Buffer.from('binary string'),
      );
      expect(await umi.fromUmiValue({ __type: 'unixtime', seconds: '1567537435' })).toEqual(
        new Date('2019-09-03T19:03:55.000Z'),
      );
      expect(await umi.fromUmiValue({ __type: 'RemoteObject', className: 'PrivateKey', id: 1 })).toBeInstanceOf(
        RemoteObject,
      );
      expect(await umi.fromUmiValue([1, 'string', true])).toStrictEqual([1, 'string', true]);
      expect(await umi.fromUmiValue({ number: 1, string: 'string', boolean: true })).toStrictEqual({
        number: 1,
        string: 'string',
        boolean: true,
      });
    });
  });
});
