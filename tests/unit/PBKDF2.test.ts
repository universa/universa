import { PBKDF2 } from '../../src/PBKDF2';
import { remoteCommandHandler } from './helpers';
import { RemoteObject } from '../../src/RemoteObject';

jest.mock('child_process');

describe('PBKDF2', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(() => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.crypto.PBKDF2',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create an instance of PBKDF2', async () => {
    const spy = jest.spyOn(RemoteObject, 'create');
    const pbkdf2 = await PBKDF2.create();
    expect(pbkdf2).toBeInstanceOf(PBKDF2);
    expect(spy).toBeCalledTimes(2);
  });

  it('should derive PBKDF2 from a string', async () => {
    const spy = jest.spyOn(RemoteObject, 'invoke');
    const pbkdf2 = await PBKDF2.derive('password');
    expect(pbkdf2).toBeInstanceOf(PBKDF2);
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith(
      'derive',
      'com.icodici.crypto.digest.Sha256',
      'password',
      Buffer.from('default_salt'),
      50000,
      32,
    );
  });

  it('should derive PBKDF2 from string with other parameters specified', async () => {
    const spy = jest.spyOn(RemoteObject, 'invoke');
    const pbkdf2 = await PBKDF2.derive('password', 'custom_salt', 40000, 'com.icodici.crypto.digest.Sha512', 64);
    expect(pbkdf2).toBeInstanceOf(PBKDF2);
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith(
      'derive',
      'com.icodici.crypto.digest.Sha512',
      'password',
      Buffer.from('custom_salt'),
      40000,
      64,
    );
  });
});
