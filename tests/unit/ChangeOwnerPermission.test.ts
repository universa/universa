import { ChangeOwnerPermission } from '../../src/ChangeOwnerPermission';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('ChangeOwnerPermission', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.contract.permissions.ChangeOwnerPermission',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of ModifyDataPermission', async () => {
    const changeOwnerPermission = await ChangeOwnerPermission.create();
    expect(changeOwnerPermission).toBeInstanceOf(ChangeOwnerPermission);
  });
});
