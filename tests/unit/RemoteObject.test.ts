/* eslint-disable @typescript-eslint/no-explicit-any */
import { RemoteObjectRef } from '../../src/RemoteObjectRef';
import { RemoteObject } from '../../src/RemoteObject';
import { UMI } from '../../src/UMI';
import { UmiRef } from '../../src/types';

jest.mock('../../src/UMI');

describe('RemoteObject', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create an instance of RemoteObject out of RemoteObjectRef', async () => {
    const umi = { invoke: jest.fn() as Function } as UMI;
    const ref = {} as UmiRef;
    const remoteObjectRef = new RemoteObjectRef({ umi, ref });
    const remoteObject = await RemoteObject.create(remoteObjectRef);

    expect(remoteObject).toBeInstanceOf(RemoteObject);
    expect(remoteObject.umi).toStrictEqual(umi);
    expect((remoteObject as any).ref).toStrictEqual(remoteObjectRef);
    expect(remoteObject.getRef(umi)).toStrictEqual(ref);

    await (remoteObject as any).invoke('someMethod', 'arg1', 'arg2');

    expect(umi.invoke).toBeCalledTimes(1);
    expect(umi.invoke).toBeCalledWith(remoteObjectRef, 'someMethod', 'arg1', 'arg2');
  });

  it('should create an instance of remoteClass', async () => {
    class RemoteObjectChild extends RemoteObject {
      protected static remoteClassName = 'remote.class.name';
    }

    await RemoteObjectChild.create('arg1', 'arg2');

    expect(UMI.instantiate).toBeCalledTimes(1);
    expect(UMI.instantiate).toBeCalledWith('remote.class.name', 'arg1', 'arg2');
  });

  it(`should throw when calling create but 'remoteClassName' static property is not defined`, async () => {
    await expect(RemoteObject.create('arg1')).rejects.toThrow();
  });

  it('should throw when calling the constructor directly', async () => {
    await expect(() => {
      new (RemoteObject as any)();
    }).toThrow();
  });

  it(`should call a singleton UMI instance when static 'invoke' method is used`, async () => {
    class RemoteObjectChild extends RemoteObject {
      protected static remoteClassName = 'remote.class.name';
    }

    await RemoteObjectChild.invoke('someStaticMethod', 'arg1', 'arg2');

    expect(UMI.invoke).toBeCalledTimes(1);
    expect(UMI.invoke).toBeCalledWith('remote.class.name', 'someStaticMethod', 'arg1', 'arg2');
  });

  it(`should call 'invoke' if method was not found on the object itself`, async () => {
    const umi = { invoke: jest.fn() as Function } as UMI;
    const ref = {} as UmiRef;
    const remoteObjectRef = new RemoteObjectRef({ umi, ref });
    const remoteObject = await RemoteObject.create(remoteObjectRef);

    await (remoteObject as any).exoticMethod('arg1', 'arg2');

    expect(umi.invoke).toBeCalledTimes(1);
    expect(umi.invoke).toBeCalledWith(remoteObjectRef, 'exoticMethod', 'arg1', 'arg2');
  });

  it('should register a proxy class only once', async () => {
    class ProxyClass extends RemoteObject {
      protected static remoteClassName = 'remote.class.name';
    }
    RemoteObject.registerProxy(ProxyClass);
    expect((RemoteObject as any).knownProxies.get('remote.class.name')).toStrictEqual(ProxyClass);
    expect(() => {
      RemoteObject.registerProxy(ProxyClass);
    }).toThrow();
  });
});
