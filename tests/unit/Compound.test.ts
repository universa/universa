import { Compound } from '../../src/Compound';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('Compound', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.contract.helpers.Compound',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of Compound', async () => {
    const compound = await Compound.create();
    expect(compound).toBeInstanceOf(Compound);
  });
});
