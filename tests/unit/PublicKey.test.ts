import { KeyAddress } from '../../src/KeyAddress';
import { PublicKey } from '../../src/PublicKey';
import { RemoteObject } from '../../src/RemoteObject';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('PublicKey', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(() => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.crypto.PublicKey',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should instantiate an instance of PublicKey', async () => {
    const spy = jest.spyOn(RemoteObject, 'create');
    const publicKey = await PublicKey.create();
    expect(publicKey).toBeInstanceOf(PublicKey);
    expect(spy).toBeCalledTimes(2);
  });

  it("should instantiate with 'fromPacked'", async () => {
    const spy = jest.spyOn(RemoteObject, 'create');
    const buf = Buffer.from('string');
    const publicKey = await PublicKey.fromPacked(buf);
    expect(publicKey).toBeInstanceOf(PublicKey);
    expect(spy).toBeCalledTimes(2);
    expect(spy).toBeCalledWith(buf);
  });

  it("should instantiate with 'fromPacked' protected by password", async () => {
    const spy = jest.spyOn(RemoteObject, 'invoke');
    const buf = Buffer.from('string');
    const publicKey = await PublicKey.fromPacked(buf, 'password');
    expect(publicKey).toBeInstanceOf(PublicKey);
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith('unpackWithPassword', buf, 'password');
  });

  describe('addresses and bit strength', () => {
    it('should return a short address', async () => {
      remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
        res.result = {
          __type: 'RemoteObject',
          className: 'com.icodici.crypto.KeyAddress',
          id: req.id,
        };
      });

      const publicKey = (await PublicKey.create(2048)) as any;
      const spy = jest.spyOn(publicKey, 'invoke');
      const shortAddress = await publicKey.getShortAddress();
      expect(shortAddress).toBeInstanceOf(KeyAddress);
      expect(publicKey._shortAddress).toStrictEqual(shortAddress);
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('getShortAddress');
      await publicKey.getShortAddress();
      expect(spy).toBeCalledTimes(1);
    });

    it('should return a long address', async () => {
      remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
        res.result = {
          __type: 'RemoteObject',
          className: 'com.icodici.crypto.KeyAddress',
          id: req.id,
        };
      });
      const publicKey = (await PublicKey.create()) as any;
      const spy = jest.spyOn(publicKey, 'invoke');
      const longAddress = await publicKey.getLongAddress();
      expect(longAddress).toBeInstanceOf(KeyAddress);
      expect(publicKey._longAddress).toStrictEqual(longAddress);
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('getLongAddress');
      await publicKey.getLongAddress();
      expect(spy).toBeCalledTimes(1);
    });

    it('should return a bit strength', async () => {
      remoteCommandHandler.mockImplementationOnce(async (_, res: any) => {
        res.result = 2048;
      });
      const publicKey = (await PublicKey.create()) as any;
      const spy = jest.spyOn(publicKey, 'invoke');
      const bitStrength = await publicKey.getBitStrength();
      expect(bitStrength).toStrictEqual(2048);
      expect(publicKey._bitStrength).toStrictEqual(bitStrength);
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('getBitStrength');
      await publicKey.getBitStrength();
      expect(spy).toBeCalledTimes(1);
    });
  });

  it('should verify a value against itself', async () => {
    remoteCommandHandler.mockImplementationOnce(async (_, res: any) => {
      res.result = true;
    });
    const publicKey = (await PublicKey.create()) as any;
    const spy = jest.spyOn(publicKey, 'invoke');
    await publicKey.verify('string', 'signature');
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith('verify', Buffer.from('string'), 'signature', 'SHA3_384');
    await publicKey.verify(Buffer.from('string'), 'signature', 'SHA2_256');
    expect(spy).toBeCalledWith('verify', Buffer.from('string'), 'signature', 'SHA2_256');
  });

  it('should encrypt a value with itself', async () => {
    remoteCommandHandler.mockImplementationOnce(async (_, res: any) => {
      res.result = Buffer.from('encoded string').toString('base64');
    });
    const publicKey = (await PublicKey.create()) as any;
    const spy = jest.spyOn(publicKey, 'invoke');
    await publicKey.encrypt('string');
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith('encrypt', Buffer.from('string'));
    await publicKey.encrypt(Buffer.from('string'));
    expect(spy).toBeCalledWith('encrypt', Buffer.from('string'));
  });
});
