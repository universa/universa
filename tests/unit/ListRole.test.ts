import { ListRole } from '../../src/ListRole';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('ListRole', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.contract.roles.ListRole',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of ListRole', async () => {
    const listRole = await ListRole.create();
    expect(listRole).toBeInstanceOf(ListRole);
  });
});
