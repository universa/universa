import { KeyInfo } from '../../src/KeyInfo';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('KeyInfo', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.crypto.KeyInfo',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of KeyInfo', async () => {
    const compound = await KeyInfo.create();
    expect(compound).toBeInstanceOf(KeyInfo);
  });
});
