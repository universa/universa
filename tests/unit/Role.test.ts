import { Role } from '../../src/Role';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('Role', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.contract.roles.Role',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of KeyInfo', async () => {
    const role = await Role.create();
    expect(role).toBeInstanceOf(Role);
  });
});
