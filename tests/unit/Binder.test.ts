import { Binder } from '../../src/Binder';
import { UMI } from '../../src/UMI';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('Binder', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'net.sergeych.tools.Binder',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of Binder', async () => {
    const binder = await Binder.create();
    expect(binder).toBeInstanceOf(Binder);
  });

  it('should create an instance of Binder via UMI.instantiate', async () => {
    const binder = await UMI.instantiate('Binder');
    expect(binder).toBeInstanceOf(Binder);
  });
});
