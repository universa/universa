import { SimpleRole } from '../../src/SimpleRole';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('SimpleRole', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.contract.roles.SimpleRole',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of ModifyDataPermission', async () => {
    const simpleRole = await SimpleRole.create();
    expect(simpleRole).toBeInstanceOf(SimpleRole);
  });
});
