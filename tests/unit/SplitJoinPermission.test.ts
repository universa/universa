import { SplitJoinPermission } from '../../src/SplitJoinPermission';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('SplitJoinPermission', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.contract.permissions.SplitJoinPermission',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of ModifyDataPermission', async () => {
    const splitJoinPermission = await SplitJoinPermission.create();
    expect(splitJoinPermission).toBeInstanceOf(SplitJoinPermission);
  });
});
