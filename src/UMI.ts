import { ChildProcess, spawn } from 'child_process';
import { Debugger } from 'debug';
import { EventEmitter } from 'events';
import { satisfies } from 'semver';
import { DEFAULT_UMI_OPTIONS } from './constants';
import { InterchangeError } from './errors';
import { logger } from './logger';
import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';
import {
  JsArray,
  JsObject,
  JsValue,
  UmiArray,
  UmiBinary,
  UmiCall,
  UmiCreateOptions,
  UmiHash,
  UmiRef,
  UmiResponse,
  UmiTime,
  UmiValue,
  UmiVersion,
  UmiOptions,
} from './types';
import { isPlainObject, sleep } from './util';

const debug = logger.extend('umi');

let instance: UMI;

export class UMI extends EventEmitter {
  private _calls: Map<number, UmiCall>;
  private _closed: boolean;
  private _coreVersion!: string;
  private _debug: Debugger;
  private _debugCall: Debugger;
  private _lastCallAt = 0;
  private _serial: number;
  private _subprocess: ChildProcess;
  private _version!: UmiVersion;

  /**
   * Returns a singleton instance of UMI
   */
  private static async _getInstance(): Promise<UMI> {
    if (!instance) {
      instance = await this.create();
    }
    return instance;
  }

  /**
   * A shorthand for `instantiate` method of the singleton UMI instance
   */
  public static async instantiate(remoteClassName: string, ...args: JsValue[]): Promise<RemoteObject> {
    const instance = await this._getInstance();
    return instance.instantiate(remoteClassName, ...args);
  }

  /**
   * A shorthand for `invoke` method of the singleton UMI instance
   */
  public static async invoke(
    remoteObject: string | RemoteObjectRef,
    method: string,
    ...args: JsValue[]
  ): Promise<JsValue> {
    const instance = await this._getInstance();
    return instance.invoke(remoteObject, method, ...args);
  }

  /**
   * A shorthand for `invokeStatic` method of the singleton instance
   */
  public static async invokeStatic(remoteClassName: string, method: string, ...args: JsValue[]): Promise<JsValue> {
    const instance = await this._getInstance();
    return instance.invokeStatic(remoteClassName, method, ...args);
  }

  /**
   * A shorthand for `getProperty` method of the singleton instance
   */
  public static async getProperty(remoteObject: RemoteObjectRef, property: string): Promise<JsValue> {
    const instance = await this._getInstance();
    return instance.getProperty(remoteObject, property);
  }

  /**
   * A shorthand for `setProperty` method of the singleton instance
   */
  public static async setProperty(remoteObject: RemoteObjectRef, property: string, value: JsValue): Promise<void> {
    const instance = await this._getInstance();
    return instance.setProperty(remoteObject, property, value);
  }

  public static async create(options: UmiCreateOptions = {}): Promise<UMI> {
    const umiOptions = {
      ...DEFAULT_UMI_OPTIONS,
      ...options,
    } as UmiOptions;

    debug('Instantiating UMI with options: %O', umiOptions);
    const umi = new UMI(umiOptions);

    umi._version = (await umi.call('version')) as UmiVersion;

    if (umi._version.system !== umiOptions.system) {
      await umi.close();
      throw new Error(`Unsupported system: '${umiOptions.system}'`);
    }
    if (!satisfies(umi._version.version, umiOptions.version)) {
      await umi.close();
      throw new Error(`Unsupported version: '${umiOptions.version}'`);
    }

    return umi;
  }

  constructor(options: UmiOptions) {
    super();

    this._subprocess = this.runSubprocess(options.binaryPath);
    this._serial = 0;
    this._calls = new Map();
    this._closed = false;
    this._debug = debug.extend(`<${this._subprocess.pid}>`);
    this._debugCall = this._debug.extend('call');
  }

  protected runSubprocess(binaryPath: string) {
    debug('Spawning UMI subprocess %s', binaryPath);
    const proc = spawn(binaryPath);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    proc.stdout!.on('data', this.handleResponse.bind(this));
    proc.on('exit', async (code: number, signal: string) => {
      if (code === 143 && !signal) {
        this.emit('error', new Error('UMI subprocess terminated from the outside.'));
      } else {
        this.emit('error', new Error(`UMI subprocess terminated abnormally. Code: ${code}. Signal: ${signal}.`));
      }
      return this.close();
    });
    proc.unref();

    return proc;
  }

  public async close(): Promise<void> {
    debug('Closing UMI interface');
    this._closed = true;

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    this._subprocess.stdout!.removeAllListeners();
    this._subprocess.removeAllListeners();

    /* istanbul ignore else */
    if (!this._subprocess.killed) {
      debug('Terminating UMI subprocess');
      await new Promise((resolve) => {
        this._subprocess.once('exit', () => resolve());
        this._subprocess.kill();
      });
    }
  }

  public get isClosed(): boolean {
    return this._closed;
  }

  public get version(): string {
    return this._version.version;
  }

  public async getCoreVersion(): Promise<string> {
    /* istanbul ignore else */
    if (!this._coreVersion) {
      this._coreVersion = (await this.invokeStatic('Core', 'getVersion')) as string;
    }
    return this._coreVersion;
  }

  public async instantiate(remoteClassName: string, ...args: JsValue[]): Promise<RemoteObject> {
    const encodedArgs = await this.toUmiValue(args);
    const response = await this.call('instantiate', remoteClassName, ...encodedArgs);
    const decodedResult = await this.fromUmiValue(response as UmiRef);
    return decodedResult;
  }

  public async invoke(
    remoteObject: string | RemoteObjectRef | RemoteObject,
    method: string,
    ...args: JsValue[]
  ): Promise<JsValue> {
    const encodedArgs = await this.toUmiValue(args);

    let target;
    if (remoteObject instanceof RemoteObject) {
      const remoteObjectRef = await remoteObject.getRef(this);
      target = remoteObjectRef.id;
    } else if (remoteObject instanceof RemoteObjectRef) {
      if (remoteObject.umi !== this) throw new InterchangeError();
      target = remoteObject.id;
    } else {
      target = remoteObject;
    }

    const result = (await this.call('invoke', target, method, ...encodedArgs)) as UmiValue;
    return this.fromUmiValue(result);
  }

  /**
   * @alias invoke
   */
  public async invokeStatic(remoteClassName: string, method: string, ...args: JsValue[]): Promise<JsValue> {
    return this.invoke(remoteClassName, method, ...args);
  }

  public async getProperty(remoteObject: RemoteObjectRef | RemoteObject, property: string): Promise<JsValue> {
    let remoteObjectId: number;
    if (remoteObject instanceof RemoteObject) {
      const remoteObjectRef = await remoteObject.getRef(this);
      remoteObjectId = remoteObjectRef.id;
    } else {
      if (remoteObject.umi !== this) throw new InterchangeError();
      remoteObjectId = remoteObject.id;
    }

    const result = (await this.call('get_field', remoteObjectId, property)) as UmiValue;
    return this.fromUmiValue(result);
  }

  /**
   * @alias getProperty
   */
  public async getField(remoteObject: RemoteObjectRef, property: string): Promise<JsValue> {
    return this.getProperty(remoteObject, property);
  }

  public async setProperty(
    remoteObject: RemoteObjectRef | RemoteObject,
    property: string,
    value: JsValue,
  ): Promise<void> {
    let remoteObjectId: number;
    if (remoteObject instanceof RemoteObject) {
      const remoteObjectRef = await remoteObject.getRef(this);
      remoteObjectId = remoteObjectRef.id;
    } else {
      if (remoteObject.umi !== this) throw new InterchangeError();
      remoteObjectId = remoteObject.id;
    }

    const encodedValue = await this.toUmiValue(value);
    await this.call('set_field', remoteObjectId, property, encodedValue);
  }

  /**
   * @alias setProperty
   */
  public async setField(remoteObject: RemoteObjectRef, property: string, value: JsValue) {
    return this.setProperty(remoteObject, property, value);
  }

  private async toUmiValue(value: JsArray): Promise<UmiArray>;
  private async toUmiValue(value: JsObject): Promise<UmiHash>;
  private async toUmiValue(value: Buffer): Promise<UmiBinary>;
  private async toUmiValue(value: Date): Promise<UmiTime>;
  private async toUmiValue(value: RemoteObject | RemoteObjectRef): Promise<UmiRef>;
  private async toUmiValue(value: Set<RemoteObject> | Set<RemoteObjectRef>): Promise<UmiRef>;
  private async toUmiValue(value: string): Promise<string>;
  private async toUmiValue(value: number): Promise<number>;
  private async toUmiValue(value: boolean): Promise<boolean>;
  private async toUmiValue(value: null | undefined): Promise<null>;
  private async toUmiValue(value: JsValue): Promise<UmiValue>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private async toUmiValue(value: any): Promise<any> {
    if (value == null) return null;

    if (Array.isArray(value)) {
      return Promise.all(value.map(this.toUmiValue.bind(this)));
    }

    if (value instanceof RemoteObjectRef || value instanceof RemoteObject) {
      return value.getRef(this) as UmiRef;
    }

    if (value instanceof Date) {
      return { __type: 'unixtime', seconds: (value.getTime() / 1000) | 0 } as UmiTime;
    }

    if (value instanceof Buffer) {
      return { __type: 'binary', base64: value.toString('base64') } as UmiBinary;
    }

    if (value instanceof Set) {
      const set = [...value].map((item: RemoteObject | RemoteObjectRef) => item.getRef(this));
      return this.call('instantiate', 'Set', set);
    }

    if (isPlainObject(value)) {
      const result: UmiHash = {};
      for (const [key, val] of Object.entries(value as JsObject)) {
        result[key] = await this.toUmiValue(val as any); // eslint-disable-line @typescript-eslint/no-explicit-any
      }
      return result;
    }

    return value;
  }

  private async fromUmiValue(value: UmiArray): Promise<JsArray>;
  private async fromUmiValue(value: UmiHash): Promise<JsObject>;
  private async fromUmiValue(value: UmiBinary): Promise<Buffer>;
  private async fromUmiValue(value: UmiTime): Promise<Date>;
  private async fromUmiValue(value: UmiRef): Promise<RemoteObject>;
  private async fromUmiValue(value: string): Promise<string>;
  private async fromUmiValue(value: number): Promise<number>;
  private async fromUmiValue(value: boolean): Promise<boolean>;
  private async fromUmiValue(value: null): Promise<null>;
  private async fromUmiValue(value: UmiValue): Promise<JsValue>;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private async fromUmiValue(value: any): Promise<any> {
    if (isPlainObject(value)) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      switch (value!.__type) {
        case 'RemoteObject':
          const remoteObjectRef = new RemoteObjectRef({ umi: this, ref: value });
          return RemoteObject.create(remoteObjectRef);
        case 'binary':
          return Buffer.from((value as UmiBinary).base64, 'base64');
        case 'unixtime':
          return new Date((value as UmiTime).seconds * 1000);
        default:
          const result: JsObject = {};
          for (const [key, val] of Object.entries(value)) {
            result[key] = await this.fromUmiValue(val as any); // eslint-disable-line @typescript-eslint/no-explicit-any
          }
      }
    } else if (Array.isArray(value)) {
      return Promise.all(value.map(this.fromUmiValue.bind(this)));
    }

    return value;
  }

  protected async call(action: string, ...args: UmiValue[]): Promise<unknown> {
    if (this._closed) throw new Error('UMI interface is closed.');

    const now = Date.now();
    if (now - this._lastCallAt < 100) {
      await sleep(100 - now + this._lastCallAt);
    }

    const commandSerial = this._serial++;
    const command = {
      serial: commandSerial,
      cmd: action,
      args,
    };
    this._debugCall('>> %o', command);

    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      this._subprocess.stdin!.write(`${JSON.stringify(command)}\n`);
      this._lastCallAt = Date.now();
      this._calls.set(commandSerial, { resolve, reject });
    }).finally(() => {
      this._calls.delete(commandSerial);
    });
  }

  private handleResponse(data: Buffer): void {
    let response: UmiResponse;

    try {
      response = JSON.parse(data.toString());
    } catch (err) {
      // Sometimes there is an empty line in umi response. Just skip it for now.
      /* istanbul ignore next */
      this._debug('Got empty response: %s', data.toString());
      return;
    }

    const call = this._calls.get(response.ref);

    if (call) {
      if (response.error) {
        this._debugCall('<< ERROR %o', response);
        call.reject(new Error(response.error.text));
      } else {
        this._debugCall('<< %o', response);
        call.resolve(response.result);
      }
    } else {
      this._debugCall('<< UNEXPECTED %o', response);
    }
  }
}
