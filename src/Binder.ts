import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';
import { JsValue } from './types';

export class Binder extends RemoteObject {
  [property: string]: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  protected static readonly remoteClassName = 'net.sergeych.tools.Binder';

  public static async create(remoteObject: RemoteObjectRef): Promise<Binder>;
  public static async create(...args: any[]): Promise<Binder>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<Binder> {
    return super.create.call(this, ...args) as Promise<Binder>;
  }

  public static async of(...args: any[]): Promise<Binder> {
    return this.invoke('of', ...args) as Promise<Binder>;
  }

  public async get(key: string): Promise<JsValue> {
    return this.invoke('get', key) as Promise<JsValue>;
  }

  public async set(key: string, value: JsValue): Promise<void> {
    await this.invoke('set', key, value);
  }

  public async getKeys(): Promise<string[]> {
    return this.invoke('keySet') as Promise<string[]>;
  }

  public async forEach(callback: Function) {
    const keys = await this.getKeys();
    await Promise.all(
      keys.map(async (key) => {
        const value = await this.get(key);
        await callback(key, value);
      }),
    );
  }

  public async map(callback: Function) {
    const keys = await this.getKeys();
    return Promise.all(
      keys.map(async (key) => {
        const value = await this.get(key);
        return callback(key, value);
      }),
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public async toArray(): Promise<Array<[string, any]>> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return this.map((key: string, value: any) => [key, value]);
  }

  public async toObject() {
    const array = await this.toArray();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const object: { [key: string]: any } = {};
    for (const [key, value] of array) {
      object[key] = value;
    }
    return object;
  }

  public async toJSON() {
    return JSON.stringify(await this.toObject());
  }
}

RemoteObject.registerProxy(Binder);
