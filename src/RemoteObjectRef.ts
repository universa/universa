import { InterchangeError } from './errors';
import { RemoteObjectRefOptions, UmiRef } from './types';
import { UMI } from './UMI';

/**
 * This is a helper class. It is intended to tie together remote object reference and UMI instance which created the remote object
 */
export class RemoteObjectRef {
  private _umi: UMI;
  private _ref: UmiRef;
  private _id: number;

  constructor(options: RemoteObjectRefOptions) {
    this._umi = options.umi;
    this._ref = options.ref;
    this._id = this._ref.id;
  }

  /* eslint-disable @typescript-eslint/no-non-null-assertion */
  public get umi(): UMI {
    return this._umi;
  }

  public get ref(): UmiRef {
    return this._ref;
  }

  public get id(): UmiRef['id'] {
    return this._id;
  }

  public get remoteClassName(): string | undefined {
    return this._ref.className;
  }
  /* eslint-enable @typescript-eslint/no-non-null-assertion */

  public getRef(umi: UMI): UmiRef {
    if (this.umi !== umi) throw new InterchangeError();
    return this.ref;
  }
}
