import { KeyAddress } from './KeyAddress';
import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class PublicKey extends RemoteObject {
  protected static remoteClassName = 'com.icodici.crypto.PublicKey';

  private _shortAddress?: KeyAddress = undefined;
  private _longAddress?: KeyAddress = undefined;
  private _bitStrength?: number = undefined;

  public static async create(remoteObject: RemoteObjectRef): Promise<PublicKey>;
  public static async create(...args: any[]): Promise<PublicKey>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<PublicKey> {
    return super.create(...args) as Promise<PublicKey>;
  }

  public static async fromPacked(packed: Buffer, password?: string): Promise<PublicKey> {
    if (password) {
      const remoteObject = (await this.invoke('unpackWithPassword', packed, password)) as RemoteObject;
      return this.create(remoteObject.ref);
    }
    return this.create(packed);
  }

  public async getShortAddress(): Promise<KeyAddress> {
    if (!this._shortAddress) {
      this._shortAddress = (await this.invoke('getShortAddress')) as KeyAddress;
    }
    return this._shortAddress;
  }

  public async getLongAddress(): Promise<KeyAddress> {
    if (!this._longAddress) {
      this._longAddress = (await this.invoke('getLongAddress')) as KeyAddress;
    }
    return this._longAddress;
  }

  public async getBitStrength(): Promise<number> {
    if (!this._bitStrength) {
      this._bitStrength = (await this.invoke('getBitStrength')) as number;
    }
    return this._bitStrength;
  }

  public async verify(data: string | Buffer, signature: string, hashType = 'SHA3_384'): Promise<boolean> {
    const buf = typeof data === 'string' ? Buffer.from(data) : data;
    return this.invoke('verify', buf, signature, hashType) as Promise<boolean>;
  }

  public async encrypt(data: string | Buffer): Promise<string> {
    const buf = typeof data === 'string' ? Buffer.from(data) : data;
    return this.invoke('encrypt', buf) as Promise<string>;
  }
}

RemoteObject.registerProxy(PublicKey);
