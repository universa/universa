import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class Reference extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.Reference';

  public static async create(remoteObject: RemoteObjectRef): Promise<Reference>;
  public static async create(...args: any[]): Promise<Reference>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<Reference> {
    return super.create.call(this, ...args) as Promise<Reference>;
  }
}

RemoteObject.registerProxy(Reference);
