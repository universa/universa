import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class UmiClient extends RemoteObject {
  [property: string]: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  protected static remoteClassName = 'com.icodici.universa.node2.network.Client';
  private _size?: number = undefined;

  public static async create(remoteObject: RemoteObjectRef): Promise<UmiClient>;
  public static async create(...args: any[]): Promise<UmiClient>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<UmiClient> {
    return super.create.call(this, ...args) as Promise<UmiClient>;
  }

  public async getSize(): Promise<number> {
    if (!this._size) {
      this._size = (await this.invoke('size')) as number;
    }
    return this._size;
  }
}

RemoteObject.registerProxy(UmiClient);
