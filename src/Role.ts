import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class Role extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.roles.Role';

  public static async create(remoteObject: RemoteObjectRef): Promise<Role>;
  public static async create(...args: any[]): Promise<Role>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<Role> {
    return super.create.call(this, ...args) as Promise<Role>;
  }
}

RemoteObject.registerProxy(Role);
