import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class ChangeOwnerPermission extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.permissions.ChangeOwnerPermission';

  public static async create(remoteObject: RemoteObjectRef): Promise<ChangeOwnerPermission>;
  public static async create(...args: any[]): Promise<ChangeOwnerPermission>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<ChangeOwnerPermission> {
    return super.create.call(this, ...args) as Promise<ChangeOwnerPermission>;
  }
}

RemoteObject.registerProxy(ChangeOwnerPermission);
