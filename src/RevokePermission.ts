import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class RevokePermission extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.permissions.RevokePermission';

  public static async create(remoteObject: RemoteObjectRef): Promise<RevokePermission>;
  public static async create(...args: any[]): Promise<RevokePermission>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<RevokePermission> {
    return super.create.call(this, ...args) as Promise<RevokePermission>;
  }
}

RemoteObject.registerProxy(RevokePermission);
