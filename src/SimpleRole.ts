import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class SimpleRole extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.roles.SimpleRole';

  public static async create(remoteObject: RemoteObjectRef): Promise<SimpleRole>;
  public static async create(...args: any[]): Promise<SimpleRole>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<SimpleRole> {
    return super.create.call(this, ...args) as Promise<SimpleRole>;
  }
}

RemoteObject.registerProxy(SimpleRole);
