import { Contract } from './Contract';
import { ContractState } from './ContractState';
import { UmiClient } from './UmiClient';
import { HashId } from './HashId';
import { Binder } from './Binder';

export class Connection {
  private _nodeNumber?: number;
  private _url?: string;

  constructor(private readonly _client: UmiClient) {}

  public get umiClient() {
    return this._client;
  }

  public async getNodeNumber(): Promise<number> {
    if (!this._nodeNumber) {
      this._nodeNumber = (await this._client.getNodeNumber()) as number;
    }
    return this._nodeNumber;
  }

  public async ping(): Promise<boolean> {
    return this._client.ping() as boolean;
  }

  public async restart(): Promise<void> {
    await this._client.restart();
  }

  public async getUrl(): Promise<string> {
    if (!this._url) {
      this._url = (await this._client.getUrl()) as string;
    }
    return this._url;
  }

  public async pingNode(nodeNumber: number, timeout = 5000): Promise<Binder> {
    return this._client.pingNode(nodeNumber, timeout) as Promise<Binder>;
  }

  public async registerSingle(contract: Contract, timeout = 25000): Promise<ContractState> {
    const packed = await contract.getPacked();
    const state = await this._client.register(packed, timeout);
    return new ContractState(state);
  }

  public async getState(object: Contract | HashId | Buffer | string): Promise<ContractState> {
    let hashId: HashId;

    if (object instanceof HashId) {
      hashId = object;
    } else if (object instanceof Contract) {
      hashId = await object.getHashId();
    } else if (object instanceof Buffer) {
      hashId = await HashId.fromDigest(object);
    } else if (typeof object === 'string') {
      hashId = await HashId.fromString(object);
    } else {
      throw new TypeError();
    }

    const state = await this._client.getState(hashId);
    return new ContractState(state);
  }
}
