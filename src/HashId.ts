import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class HashId extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.HashId';

  public static async create(remoteObject: RemoteObjectRef): Promise<HashId>;
  public static async create(...args: any[]): Promise<HashId>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<HashId> {
    return super.create.call(this, ...args) as Promise<HashId>;
  }

  public static async fromDigest(digest: Buffer): Promise<HashId> {
    return this.invoke('withDigest', digest) as Promise<HashId>;
  }

  public static async fromString(string: string): Promise<HashId> {
    const digest = Buffer.from(string.replace(/-/g, '+').replace(/_/g, '/'));
    return this.fromDigest(digest);
  }

  public static async createRandom(): Promise<HashId> {
    return this.invoke('createRandom') as Promise<HashId>;
  }

  public _bytes?: Buffer = undefined;
  public async getBytes(): Promise<Buffer> {
    if (!this._bytes) {
      this._bytes = (await this.invoke('getDigest')) as Buffer;
    }
    return this._bytes;
  }

  public _string?: string = undefined;
  public async toString(): Promise<string> {
    if (!this._string) {
      const bytes = await this.getBytes();
      this._string = bytes.toString('base64').replace(/\s/g, '');
    }
    return this._string;
  }

  public _urlSafeString?: string;
  public async toUrlSafeString(): Promise<string> {
    if (!this._urlSafeString) {
      const string = await this.toString();
      this._urlSafeString = string.replace(/\\/g, '').replace(/\+/g, '');
    }
    return this._urlSafeString;
  }

  // public async getHash() {}

  public async isEqual(hash: string | Buffer | HashId) {
    if (!hash) return false;
  }
}

RemoteObject.registerProxy(HashId);
