import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class RoleLink extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.roles.RoleLink';

  public static async create(remoteObject: RemoteObjectRef): Promise<RoleLink>;
  public static async create(...args: any[]): Promise<RoleLink>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<RoleLink> {
    return super.create.call(this, ...args) as Promise<RoleLink>;
  }
}

RemoteObject.registerProxy(RoleLink);
