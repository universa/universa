import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class Compound extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.helpers.Compound';

  public static async create(remoteObject: RemoteObjectRef): Promise<Compound>;
  public static async create(...args: any[]): Promise<Compound>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<Compound> {
    return super.create.call(this, ...args) as Promise<Compound>;
  }
}

RemoteObject.registerProxy(Compound);
