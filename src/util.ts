/* istanbul ignore file */

/**
 * Implementation of `isPlainObject` (and friends) is taken from https://github.com/lodash/lodash
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getTag = (value: any) => {
  if (value == null) {
    return value === undefined ? '[object Undefined]' : '[object Null]';
  }
  return toString.call(value);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const isObjectLike = (value: any) => {
  return typeof value === 'object' && value !== null;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isPlainObject = (value: any) => {
  if (!isObjectLike(value) || getTag(value) !== '[object Object]') {
    return false;
  }
  if (Object.getPrototypeOf(value) === null) {
    return true;
  }
  let proto = value;
  while (Object.getPrototypeOf(proto) !== null) {
    proto = Object.getPrototypeOf(proto);
  }
  return Object.getPrototypeOf(value) === proto;
};
/** / */

export const sleep = (ms: number): Promise<void> => new Promise((resolve) => setTimeout(resolve, ms));

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getPropertyDescriptors = (object: any, RootClass: Function = Object) => {
  const propertyDescriptors: { [k: string]: PropertyDescriptor } = Object.create(null);
  let currentObject;

  do {
    currentObject = Reflect.getPrototypeOf(currentObject || object);
    const descriptors = Object.getOwnPropertyDescriptors(currentObject);
    for (const propertyName of Object.keys(descriptors)) {
      if (!propertyDescriptors[propertyName]) {
        propertyDescriptors[propertyName] = descriptors[propertyName];
      }
    }
  } while (currentObject.constructor !== RootClass);

  return propertyDescriptors;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getObjectMethods = (object: any, RootClass: Function = Object) => {
  const methods: Set<string> = new Set();
  const propertyDescriptors = getPropertyDescriptors(object, RootClass);

  for (const propertyName of Object.keys(propertyDescriptors)) {
    if (typeof propertyDescriptors[propertyName].value === 'function' && propertyName !== 'constructor') {
      methods.add(propertyName);
    }
  }

  return [...methods];
};

export const hasProperty = (target: object, property: string | number | symbol) => {
  if (Object.prototype.hasOwnProperty.call(target, property)) {
    return true;
  }

  const properties = getPropertyDescriptors(target);
  return Object.keys(properties).some((propertyName) => {
    const prop = properties[propertyName];
    return (
      (typeof prop.get === 'function' || typeof prop.set === 'function' || typeof prop.value === 'function') &&
      propertyName === (property as string)
    );
  });
};

export async function retry<T>(fn: Function, timeout = 25000, retries = 3): Promise<T | void> {
  let retry = 1;
  do {
    try {
      return await Promise.race([
        fn(),
        new Promise((_, reject) => {
          setTimeout(reject, timeout).unref();
        }),
      ]);
    } catch (err) {}
  } while (retry++ < retries);
}
