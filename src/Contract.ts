import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';
import { Role } from './Role';
import { KeyAddress } from './KeyAddress';
import { PublicKey } from './PublicKey';
import { HashId } from './HashId';
import { Binder } from './Binder';
import { PrivateKey } from './PrivateKey';

export class Contract extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.Contract';

  public static async create(remoteObject: RemoteObjectRef): Promise<Contract>;
  public static async create(...args: any[]): Promise<Contract>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<Contract> {
    return super.create.call(this, ...args) as Promise<Contract>;
  }

  public static async fromPacked(packed: string | Buffer): Promise<Contract> {
    if (!packed) throw new Error('Packed contract required');
    if (typeof packed === 'string') {
      packed = Buffer.from(packed);
    }
    return this.invoke('fromPackedTransaction', packed) as Promise<Contract>;
  }

  public async seal(): Promise<Buffer> {
    return this.invoke('seal') as Promise<Buffer>;
  }

  public async getCreator(): Promise<Role> {
    return this.invoke('getCreator') as Promise<Role>;
  }

  public async getIssuer(): Promise<Role> {
    return this.invoke('getIssuer') as Promise<Role>;
  }

  public async getOwner(): Promise<Role> {
    return this.invoke('getOwner') as Promise<Role>;
  }

  public async setOwner(ownerKey: KeyAddress | PublicKey): Promise<void> {
    await this.invoke('setOwnerKey', ownerKey);
  }

  public async isOk(): Promise<boolean> {
    return this.invoke('isOk') as Promise<boolean>;
  }

  public async getHashId(): Promise<HashId> {
    return this.invoke('getId') as Promise<HashId>;
  }

  public async getOrigin(): Promise<HashId> {
    return this.invoke('getOrigin') as Promise<HashId>;
  }

  public async getParent(): Promise<HashId> {
    return this.invoke('getParent') as Promise<HashId>;
  }

  public async getExpiresAt(): Promise<Date> {
    return this.invoke('getExpiresAt') as Promise<Date>;
  }

  public async setExpiresAt(time: Date): Promise<void> {
    await this.invoke('setExpiresAt', time);
  }

  private _definition?: Binder = undefined;
  public async getDefinition(): Promise<Binder> {
    if (!this._definition) {
      const definition = (await this.invoke('getDefinition')) as Binder;
      const data = (await definition.getData()) as Binder;
      this._definition = data;
    }
    return this._definition;
  }

  private _state?: Binder = undefined;
  public async getState(): Promise<Binder> {
    if (!this._state) {
      this._state = (await this.invoke('getStateData')) as Binder;
    }
    return this._state;
  }

  private _transactional?: Binder = undefined;
  public async getTransactional(): Promise<Binder> {
    if (!this._transactional) {
      this._transactional = (await this.invoke('getTransactionalData')) as Binder;
    }
    return this._transactional;
  }

  public async getPacked(): Promise<Buffer> {
    return this.invoke('getPackedTransaction') as Promise<Buffer>;
  }

  public async createRevocation(...keys: PrivateKey[]): Promise<Contract> {
    const contract = (await this.umi.invoke('ContractsService', 'createRevocation', this, ...keys)) as Contract;
    await contract.seal();
    return contract;
  }
}

RemoteObject.registerProxy(Contract);
