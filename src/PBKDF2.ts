import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class PBKDF2 extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.crypto.PBKDF2';

  public static async create(remoteObject: RemoteObjectRef): Promise<PBKDF2>;
  public static async create(...args: any[]): Promise<PBKDF2>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<PBKDF2> {
    return super.create.call(this, ...args) as Promise<PBKDF2>;
  }

  public static async derive(
    password: string,
    salt: string | Buffer = Buffer.from('default_salt'),
    rounds = 50000,
    hash = 'com.icodici.crypto.digest.Sha256',
    length = 32,
  ) {
    if (typeof salt === 'string') {
      salt = Buffer.from(salt);
    }
    return this.invoke('derive', hash, password, salt, rounds, length) as Promise<PBKDF2>;
  }
}

RemoteObject.registerProxy(PBKDF2);
