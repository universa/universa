/* istanbul ignore file */
/* eslint-disable @typescript-eslint/ban-ts-ignore */
import * as createDebug from 'debug';
import { inspect } from 'util';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
createDebug.formatters.a = (array: any[]) => {
  const inspectOpts = {
    // @ts-ignore
    ...createDebug.inspectOpts,
    // @ts-ignore
    useColors: createDebug.useColors(),
  };
  return inspect(array, inspectOpts).replace(/^\[\s*|\s*\]$/g, '');
};

export const logger = createDebug('universa');
