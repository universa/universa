import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class ListRole extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.roles.ListRole';

  public static async create(remoteObject: RemoteObjectRef): Promise<ListRole>;
  public static async create(...args: any[]): Promise<ListRole>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<ListRole> {
    return super.create.call(this, ...args) as Promise<ListRole>;
  }
}

RemoteObject.registerProxy(ListRole);
