import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class TransactionPack extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.TransactionPack';

  public static async create(remoteObject: RemoteObjectRef): Promise<TransactionPack>;
  public static async create(...args: any[]): Promise<TransactionPack>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<TransactionPack> {
    return super.create.call(this, ...args) as Promise<TransactionPack>;
  }
}

RemoteObject.registerProxy(TransactionPack);
