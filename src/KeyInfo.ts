import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class KeyInfo extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.crypto.KeyInfo';

  public static async create(remoteObject: RemoteObjectRef): Promise<KeyInfo>;
  public static async create(...args: any[]): Promise<KeyInfo>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<KeyInfo> {
    return super.create.call(this, ...args) as Promise<KeyInfo>;
  }
}

RemoteObject.registerProxy(KeyInfo);
