import * as path from 'path';
import { Connection } from './Connection';
import { DEFAULT_CLIENT_CREATE_OPTIONS } from './constants';
import { Contract } from './Contract';
import { ContractState } from './ContractState';
import { PrivateConstructorError } from './errors';
import { HashId } from './HashId';
import { PrivateKey } from './PrivateKey';
import { ClientCreateOptions } from './types';
import { UmiClient } from './UmiClient';
import { retry } from './util';

const INTERNAL_CALL = Symbol('internalCall');

type ClientOptions = {
  client: UmiClient;
  size: number;
  cacheDir?: string;
  privateKey: PrivateKey;
  [INTERNAL_CALL]: boolean;
};

async function getHashId(object: Contract | HashId | Buffer | string): Promise<HashId> {
  let hashId: HashId;

  if (object instanceof HashId) {
    hashId = object;
  } else if (object instanceof Contract) {
    hashId = await object.getHashId();
  } else if (object instanceof Buffer) {
    hashId = await HashId.fromDigest(object);
  } else if (typeof object === 'string') {
    hashId = await HashId.fromString(object);
  } else {
    throw new TypeError();
  }

  return hashId;
}

export class Client {
  private _client: UmiClient;
  private _connections: Connection[];
  // XXX: The private key seems not to be used
  protected _privateKey: PrivateKey;
  public size: number;

  public static async create(_options: ClientCreateOptions = {}): Promise<Client> {
    const options = {
      ...DEFAULT_CLIENT_CREATE_OPTIONS,
      ..._options,
    } as ClientCreateOptions;

    const topology = options.topologyFile ? path.resolve(options.topologyFile) : options.topology;
    const privateKey = options.privateKey || (await PrivateKey.create(2048));
    const client = await UmiClient.create(topology, options.cacheDir, privateKey);
    const size = await client.getSize();

    return new Client({ client, size, privateKey, cacheDir: options.cacheDir, [INTERNAL_CALL]: true });
  }

  constructor(options: ClientOptions) {
    if (!options[INTERNAL_CALL]) throw new PrivateConstructorError(this.constructor.name);

    this._client = options.client;
    this._privateKey = options.privateKey;
    this.size = options.size;
    this._connections = Array(this.size).fill(null);
  }

  public async get(index: number): Promise<Connection> {
    if (index < 0 || index >= this.size) throw RangeError();
    if (!this._connections[index]) {
      const client = await this._client.getClient(index);
      this._connections[index] = new Connection(client);
    }
    return this._connections[index];
  }

  public async getRandomConnection(): Promise<Connection> {
    const index = Math.floor(Math.random() * this.size);
    return this.get(index);
  }

  public async getRandomConnections(number: number): Promise<Connection[]> {
    if (number < 1 || number > this.size) throw new RangeError();
    return Promise.all(
      [...this._connections.keys()]
        .sort(() => Math.random() - Math.random())
        .slice(0, number)
        .map(this.get.bind(this)),
    );
  }

  public async isApproved(object: Contract | HashId | Buffer | string, trust = 0.3): Promise<boolean> {
    let hashId: HashId;

    try {
      hashId = await getHashId(object);
    } catch (err) {
      console.log(err);
      throw new TypeError(`Couldn't check approval of ${object}`);
    }

    if (trust < 0.1 || trust > 0.9) {
      throw new RangeError(`'trust' must be between 0.1 and 0.9`);
    }

    return this._client.isApprovedByNetwork(hashId, trust, 60000);
  }

  public async getState(object: Contract | HashId | Buffer | string, trust = 0.3): Promise<ContractState> {
    let hashId: HashId;

    try {
      hashId = await getHashId(object);
    } catch (err) {
      throw new TypeError(`Couldn't get state for ${object}`);
    }

    if (trust < 0.1 || trust > 0.9) {
      throw new RangeError(`'trust' must be between 0.1 and 0.9`);
    }

    let negativeVotes = Math.round(this.size * 0.1) + 1;
    let positiveVotes = Math.round(this.size * trust);
    let result: ContractState | undefined;

    await Promise.all(
      [...this._connections.keys()]
        .sort(() => Math.random() - Math.random())
        .map(async (index) => {
          if (!result) {
            const connection = await this.get(index);
            const state = await connection.getState(hashId);

            if (state.approved) {
              if (--positiveVotes < 0 && !result) {
                result = state;
              }
            } else if (--negativeVotes < 0 && !result) {
              result = state;
            }
          }
        }),
    );

    return result as ContractState;
  }

  public async registerSingle(contract: Contract, timeout = 45000, retries = 3): Promise<ContractState | void> {
    return retry<ContractState>(
      async () => {
        const connection = await this.getRandomConnection();
        const state = await connection.registerSingle(contract, timeout / retries - 100);
        return new ContractState(state);
      },
      timeout,
      retries,
    );
  }
}
