import { KeyAddress } from './KeyAddress';
import { PublicKey } from './PublicKey';
import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class PrivateKey extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.crypto.PrivateKey';
  protected static localProperties = ['then'];

  private _publicKey?: PublicKey = undefined;
  private _shortAddress?: KeyAddress = undefined;
  private _longAddress?: KeyAddress = undefined;
  private _bitStrength?: number = undefined;

  public static async create(remoteObject: RemoteObjectRef): Promise<PrivateKey>;
  public static async create(...args: any[]): Promise<PrivateKey>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<PrivateKey> {
    return super.create.call(this, ...args) as Promise<PrivateKey>;
  }

  public static async fromPacked(packed: Buffer, password?: string): Promise<PrivateKey> {
    if (password) {
      const remoteObject = (await this.invoke('unpackWithPassword', packed, password)) as RemoteObject;
      return this.create(remoteObject.ref);
    }
    return this.create(packed);
  }

  public async getPublicKey(): Promise<PublicKey> {
    if (!this._publicKey) {
      this._publicKey = (await this.invoke('getPublicKey')) as PublicKey;
    }
    return this._publicKey as PublicKey;
  }

  public async getShortAddress(): Promise<KeyAddress> {
    if (!this._shortAddress) {
      const publicKey = await this.getPublicKey();
      this._shortAddress = await publicKey.getShortAddress();
    }
    return this._shortAddress;
  }

  public async getLongAddress(): Promise<KeyAddress> {
    if (!this._longAddress) {
      const publicKey = await this.getPublicKey();
      this._longAddress = await publicKey.getLongAddress();
    }
    return this._longAddress;
  }

  public async getBitStrength(): Promise<number> {
    if (!this._bitStrength) {
      const publicKey = await this.getPublicKey();
      this._bitStrength = await publicKey.getBitStrength();
    }
    return this._bitStrength;
  }

  public sign(data: string | Buffer, hashType = 'SHA3_384') {
    const buf = typeof data === 'string' ? Buffer.from(data) : data;
    return this.invoke('sign', buf, hashType);
  }
}

RemoteObject.registerProxy(PrivateKey);
